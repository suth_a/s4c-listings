$(document).ready(function(){



	// !------------------------! //
	// ! Focus parent for form  ! //
	// !------------------------! //

	$(".focus-dad").focusin(function(){
		$(this).closest(".input-holder").addClass("focus");
	})
	.focusout(function(){
		$(this).closest(".input-holder").removeClass("focus");
	});



	// !------------------------! //
	// !    Drop down control   ! //
	// !------------------------! //

	// I want the initial option to be styled differently. I think.
	function defaultOption(elem) {

		// elem is a passed select list
		var 	selectedOptionVal = elem.children("option:selected").val();

		if (selectedOptionVal == 0) {
			elem.addClass("default-selected");
		} else {
			elem.removeClass("default-selected");
		}
	}

	// set up selects
	$("select.default-option").each(function(){
		defaultOption($(this));
	});

	// check select on change
	$("select.default-option").change(function(){
		defaultOption($(this));
	});



	// !------------------------! //
	// !     Form validation    ! //
	// !------------------------! //

	// check if the form needs to be disabled
	function checkSubmit(thisForm) {
		var submitElem = thisForm.find("button:submit");

		if (thisForm.find("div.invalid-input").length) {
			// form has failed validation at some point
			// disable submit
			thisForm
				.find("button:submit")
				.addClass("disabled");
		} else {
			// now enable that shit
			thisForm
				.find("button:submit")
				.removeClass("disabled");
			submitElem.removeClass("clicked");
		}

	}


	// email validation
	function validateEmail(email) {
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	}

	function checkEmail(elem) {
		
		var email = elem.val(),
			elemParent = elem.closest("div.input-holder"),
			thisForm = elem.closest("form");

		if (email.length > 0) {
			if (validateEmail(email)) {
				// valid email
				elemParent
					.addClass("valid-input")
					.removeClass("invalid-input");
			} else {
				// invalid email string
				elemParent
					.removeClass("valid-input")
					.addClass("invalid-input");
			}
		} else {
			// nothing entered, so invalid
			elemParent
				.removeClass("valid-input")
				.removeClass("invalid-input");
		}

		checkSubmit(thisForm);

	}

	$("form.swanky-form input[type=email][required]").on('keyup focusout', function() {
		checkEmail($(this));
	});



	// Live password matching/verification
	// very specific functionality
	function checkLock(thisForm) {
		// grab the password strings from the form
		var password1 = $("#password").val(),
			password2 = $("#password2").val();

		// check password 1 / length
		if (password1.length < 6) {
			// password not long enough / remain 'unlocked'
			$("div.password1-holder").removeClass("locked valid-input");
			$("div.password1-holder").addClass("unlocked invalid-input");
		} else {
			// password long enough / lock this bitch down
			$("div.password1-holder").addClass("locked valid-input");
			$("div.password1-holder").removeClass("unlocked invalid-input");
		}

		// check password 1 and 2 match
		if (password1.length > 5) {
			// password is long enough
			if (password1 === password2) {
				// passwords match / lock everything
				$("div.password2-holder").addClass("locked valid-input");
				$("div.password2-holder").removeClass("unlocked invalid-input");
			} else {
				// passwords don't match / remain unlocked
				$("div.password2-holder").removeClass("locked valid-input");
				$("div.password2-holder").addClass("unlocked invalid-input");
			}
		} else {
			// password isn't long enough
			$("div.password2-holder").removeClass("locked valid-input");
			$("div.password2-holder").addClass("unlocked invalid-input");
		}

		checkSubmit(thisForm);

	}

	$("form.swanky-form #password").keyup(function() {
		// user has typed something in the first password box
		// fire the lock check
		var thisForm = $(this).closest("form");
		checkLock(thisForm);
	});

	$("form.swanky-form #password2").on('keyup', function() {
		// user has typed something in the second password box
		// fire the lock check
		var thisForm = $(this).closest("form");
		checkLock(thisForm);
	});


	// date validation (very simple)

	// as soon as somebody enters something or selects something
	// validation for each field kicks in

	// check if the date is > 0 and < 32
	function checkDate(elem) {
		var dateVal = elem.val(),
			elemParent = elem.closest("div.input-holder");

		if (dateVal.length) {
			if (dateVal > 0 && dateVal < 32) {
				// valid date range
				elemParent
					.addClass("valid-input")
					.removeClass("invalid-input");
	
			} else {
				// invalid date range
				elemParent
					.removeClass("valid-input")
					.addClass("invalid-input");			
			}
		} else {
			// empty string = invlaid
			elemParent
				.removeClass("valid-input")
				.addClass("invalid-input");
				
		}
	}

	// check year is > 1900 && < now
	function checkYear(elem) {

		var d = new Date(),
			yearNow = d.getFullYear(),
			yearVal = elem.val(),
			elemParent = elem.closest("div.input-holder");

		if (yearVal.length) {
			if (yearVal > 1900 && yearVal < yearNow) {
				elemParent
					.addClass("valid-input")
					.removeClass("invalid-input");
			} else {
				elemParent
					.removeClass("valid-input")
					.addClass("invalid-input");
			}
		} else {
			// empty string = invlaid
			elemParent
				.removeClass("valid-input")
				.addClass("invalid-input");

		}
	}

	// check a month is selected
	function checkMonth(elem) {
		var elemParent = elem.closest("div.input-holder"),
			selectedOptionVal = elem.children("option:selected").val();

		if (selectedOptionVal == 0 || !selectedOptionVal.length) {
			elemParent
				.removeClass("valid-input")
				.addClass("invalid-input");
		} else {
			elemParent
				.addClass("valid-input")
				.removeClass("invalid-input");
		}
	}

	function checkAllDate() {
		checkDate($("input#birth_day"));
		checkYear($("input#birth_year"));
		checkMonth($("select#birth_month"));
		checkSubmit($("form#updateMainAccountForm"));
	}

/*
	$("select#birth_month").change(function(){
		checkAllDate();
	});

	$("input#birth_day, input#birth_year").on('keyup', function() {
		checkAllDate();
	});
*/


	// check if required checkbox is checked

	function checkBoxCheck(elem) {
		// pass a checkbox element (input type=checkbox class=required)
		if (elem.is(":checked")) {
			elem
				.closest("div.checkbox-holder")
				.removeClass("invalid-input");
		} else {
			elem
				.closest("div.checkbox-holder")
				.addClass("invalid-input");
		}
		checkSubmit(elem.closest("form"));
	}

	$("form.swanky-form input").focus(function(){
		$(this).closest("form").find("div.checkbox-holder").each(function(){
			$(this).find("input:checkbox").each(function(){
				checkBoxCheck($(this));
			})
		});
	});




	// !------------------------! //
	// !   Form when disabled   ! //
	// !------------------------! //

	$("form.swanky-form button:submit").on("click", function(e){
		if ($(this).hasClass("disabled")) {
			e.preventDefault();
			$(this).addClass("clicked");
		}
	});




	// !------------------------! //
	// !   Form interactivity   ! //
	// !------------------------! //

	// checkbox toggle interaction
	function checkCheck(elem) {
		var toggleElem,
			inputElem;

		// choice of either a.toggle or
		// its label sibling being passed
		// need to determine which and
		// act accordingly

		if (elem.hasClass("toggle")) {
			toggleElem = elem;
		} else {
			toggleElem = elem.siblings("a.toggle");
		}

		inputElem = elem.siblings("input");

		// check the passed element - see if its
		// sibling input is checked. If it is,
		// throw the appropriate styles around
		if(inputElem.is(":checked")) {
			toggleElem
				.addClass("on")
				.removeClass("off");
			
		} else {
			toggleElem
				.removeClass("on")
				.addClass("off");
		}

	}

	// sort out the existing checkboxes
	$("div.checkbox-holder a.toggle").each(function(){
		checkCheck($(this));
	});

	// dealing with a click on the toggle UI
	$("div.checkbox-holder a.toggle, div.checkbox-holder label").click(function(e){
		e.preventDefault();

		var elem = $(this),
			elemInput = elem.siblings("input");

		// toggle this hidden checkbox
		elem.siblings("input").prop("checked", !$(this).siblings("input").prop("checked"));
		// now fire the stlye handling function
		checkCheck(elem);

		if (elemInput.hasClass("required")) {
			checkBoxCheck(elemInput)
		}
	});


	// radio toggle interaction
	// a bit different to the above as we have groups
	// of radio buttons and only one can be
	// selected in each group

	function checkRadios(elem) {
		// this makes sure that every time
		// a radio is pressed, each of the other
		// radios in that set are also checked
		elem.closest("div.radio-group").find("a.toggle").each(function(){
			checkCheck($(this));
		});


	}

	// initially, check each radio element in the form
	// no need for uniqueness testing yet
	$("div.radio-holder a.toggle").each(function(){
		checkCheck($(this));
	});

	// dealing with a click on the toggle UI
	$("div.radio-holder a.toggle, div.radio-holder label").click(function(e){
		e.preventDefault();
		// only do this thing if the current option is unselected
		if (!$(this).siblings("input").is(':checked')) {
			// toggle this hidden checkbox
			$(this).siblings("input").prop("checked", !$(this).siblings("input").prop("checked"));
			// now fire the stlye handling function
			checkRadios($(this));
		}
	});




	// !------------------------! //
	// ! Inline hidden controls ! //
	// !------------------------! //

	$(".toggle-hidden-controls").on("click", function(e){
		e.preventDefault();
		$(this).siblings(".hidden-controls").toggleClass("visible");
	});

	$(".hidden-controls a.close").on("click", function(e){
		e.preventDefault();
		$(this).closest(".hidden-controls").removeClass("visible");
	});




	// !------------------------! //
	// !      Toast control     ! //
	// !------------------------! //

	// close error alert on click

	$(".popup-alert a.close").on("click", function(e){
		e.preventDefault();
		bapiModule.successAlertClose();
	});


	// !------------------------! //
	// !        Tooltips        ! //
	// !------------------------! //

/*
	$('.tooltip').tooltipster({
		position: "top-right",
		maxWidth: 400,
		offsetX: 4,
		offsetY: -20
	});
*/


	// !------------------------! //
	// !      Cookie  check     ! //
	// !------------------------! //

	function are_cookies_enabled() {
		var cookieEnabled = (navigator.cookieEnabled) ? true : false;
	
		if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
			document.cookie="testcookie";
			cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
		}
		return (cookieEnabled);
	}

	if (!are_cookies_enabled()) {
		$("html").addClass("no-cookies");
	}



	// !------------------------! //
	// !      Label  links      ! //
	// !------------------------! //

	$(document).on("tap click", 'label a', function( event, data ){
		event.stopPropagation();
		event.preventDefault();
		window.location.href = $(this).attr('href');
//		window.open($(this).attr('href'), $(this).attr('target'));
		return false;
	});



});




// !------------------------! //
// !          Modul         ! //
// !------------------------! //

// confirmation popup

function confirmPopup(confirmID) {

	var elem = $("#" + confirmID);

	function showOverlay() {
		$("div.overlay").addClass("show");
	}

	function hideOverlay() {
		$("div.overlay").removeClass("show");
	}

	// hide any confirm-holders that may be open
	$("div.confirm-holder").hide();

	// show passed ID
	elem.show();

	showOverlay();

	// close if cancelled
	elem.find("a.no").on("click", function(e){
		e.preventDefault();
		hideOverlay();
	});

}




/*

function getImages() {

//	$.getJSON( "http://www.s4c.cymru/yvd_2/splash_screen", function(data) {
	// temporarily shove the category json into a local file
//	$.getJSON( "json/test.json", function(data) {
	$.getJSON( "/yvd_2/splash_screen", function(data) {

		var 	images = [];

		$.each(data.progs_featured, function(i, item) {
			images.push("<li><img src='//www.s4c.co.uk/amg/web/300x169/" + item.thumbnail_url + "' alt='" + item.programme_title + "'></li>");
		});

		$("ul.images").html(images.join(""));

	});
}
*/












