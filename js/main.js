
// CONTENTS

// ---------------------------------------
// # requestAnimationFrame
// ---------------------------------------
// a polyfill for requestAnimationFrame

// ---------------------------------------
// # Touch != Hover
// ---------------------------------------
// removing hover styles for touch
// enabled devices

// ---------------------------------------
// # Tabs
// ---------------------------------------
// sorting the tabbed content on the 
// homepage

// ---------------------------------------
// # AJAX highlights
// ---------------------------------------
// pulling in youview feed to populate
// highlights tab on the homepage
// NB: This is for demo only, live version
// will populate server side

// ---------------------------------------
// # AJAX tab genre
// ---------------------------------------
// pulling genres from Clic
// and populating tabs
// NB: This is for demo only, live version
// will populate server side

// ---------------------------------------
// # sgorio news card
// ---------------------------------------
// pulling in sgorio news feed
// and displaying 4 latest headlines
// NB: This is for demo only, live version
// will populate server side

// ---------------------------------------
// # large slider
// ---------------------------------------
// homepage slider

// ---------------------------------------
// # Language
// ---------------------------------------
// check what lang this page is

// ---------------------------------------
// # Resize Control
// ---------------------------------------
// prevents resize event firing constantly

// ---------------------------------------
// # Sticky nav
// ---------------------------------------
// control when to display stick nav

// ---------------------------------------
// # Toggle nav
// ---------------------------------------
// toggle mobile nav

// ---------------------------------------
// # Handle scroll stuff
// ---------------------------------------
// hands off scroll stuff for performance
// to avoid jank

// ---------------------------------------
// # Initialise page
// ---------------------------------------
// fires stickynav

// ---------------------------------------
// # context menu
// ---------------------------------------
// toggle control for displaying sub menu

// ---------------------------------------
// # Homepage listings
// ---------------------------------------
// pulls in listings feed and makes it
// gert lush

// ---------------------------------------
// # Corporate links
// ---------------------------------------
// simple accordian on small screens

// ---------------------------------------
// # AJAX corporate links
// ---------------------------------------
// pulling in corporate links
// from worpress and somewhere else
// NB: This is for demo only, live version
// will populate server side

// ---------------------------------------
// # mobile now/next
// ---------------------------------------
// pulling in now/next feed
// and dropping in mobile navigation
// NB: This is for demo only, live version
// will populate server side

// ---------------------------------------
// # Resize Actions
// ---------------------------------------
// Check for rezise and fire events

// ---------------------------------------
// # Advertising
// ---------------------------------------
// Advertising junk



// !-------------------------! //
// ! # requestAnimationFrame ! //
// !-------------------------! //


// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. Fixes from Paul Irish and Tino Zijdel

// MIT license

(function() {
	var lastTime = 0;
	var vendors = ['webkit', 'moz'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame =
		window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}
	if (!window.requestAnimationFrame) {
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); },
			timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}
	if (!window.cancelAnimationFrame) {
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
	}
}());




// !------------------------! //
// !   # Touch != Hover     ! //
// !------------------------! //

// Disable hover effects for touch devices


var touch = false;
if (Modernizr.touch) {
	touch = true;
}

if (touch) { // remove all :hover stylesheets
	try { // prevent crash on browsers not supporting DOM styleSheets properly
		for (var si in document.styleSheets) {
			var styleSheet = document.styleSheets[si];
			if (!styleSheet.rules) continue;

			for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
				if (!styleSheet.rules[ri].selectorText) continue;

				if (styleSheet.rules[ri].selectorText.match(':hover')) {
					styleSheet.deleteRule(ri);
				}
			}
		}
	} catch (ex) {}
}



(function($){


	// !------------------------! //
	// !       # Tabs           ! //
	// !------------------------! //

	$.fn.acTabs = function() {

		// plugin to do the tabs
		// expecting an element like this...
		
		// div.tab-container
		//   - div.tab-group
		//     - ul.tabs
		//       - li.tab (must relate precisley to the li.tab-contents below in order and amount)
		//   - ul.tab-contents-list
		//     - li.tab-contents

		var thisElem = $(this),
			thisWindow = $(window),
			tabList = thisElem.find("ul.tabs"),
			contentList = thisElem.find("ul.tab-contents-list"),
			containerHeight,
			currentTab = 0,
			animateDuration = 300,
			animating = false,
			feedURLBase =  window.location.protocol + "//" + window.location.host;


		function isLocal() {
			// am I running this locally?
			// effects cross domain bollucks is all
			if (feedURLBase === "http://webmb01.local:5757"  || feedURLBase === "http://webmb01.s4c.co.uk:5757/" || feedURLBase === "http://localhost:5757" || feedURLBase === "http://localhost:5757/") {
				return true;
			} else {
				return false;
			}
		}


		// get currentHeight
		function getHeight() {
			var currentTabH,
				containerPadding = contentList.find("li.tab-contents").css("paddingTop") + contentList.find("li.tab-contents").css("paddingBottom");
			// get height of current tab
			currentTabH = contentList.find("li.tab-contents").eq(currentTab).height() + containerPadding;
			return currentTabH;
		}

	
		// tabs setup
		function setupTabs() {
			// apply 'on' to first tab
			tabList.children("li.tab").eq(currentTab).children("a").addClass("on");
			// hide all content slides other than first
			contentList.children("li.tab-contents").hide().eq(currentTab).show();
			// set container height. It's important, that's why
			contentList.height(getHeight());
			// add class of w2 to the first item in each tab container
			contentList.children("li.tab-contents").children("div:first-child").addClass("w2");
		}

		function changeTab(thisTab) {
			var newTab;
			if (!animating) {
				animating = true;
				tabList.find("li.tab a").removeClass("on");
				thisTab.addClass("on");
				newTab = tabList.find("li.tab a.on").parent("li").index();
	
				// fade out current content
				contentList.find("li.tab-contents").eq(currentTab).fadeOut(animateDuration, function(){
					// fade in new content
					contentList.find("li.tab-contents").eq(newTab).fadeIn(animateDuration, function(){
						currentTab = newTab;
						contentList.animate({height:getHeight()},(animateDuration/2),function(){
							animating = false;
						});
					});
				});
			}
		}

		// grab the click
		tabList.on("click", "li.tab a", function(e){
			e.preventDefault();
			var thisElem = $(this);
			if (!thisElem.hasClass("on")) {
				changeTab(thisElem);
			}
		});

		// resize control
		var waitForFinalEvent = (function () {
			var timers = {};
			return function (callback, ms, uniqueId) {
				if (!uniqueId) {
					uniqueId = "Needs a unique ID";
				}
				if (timers[uniqueId]) {
					clearTimeout (timers[uniqueId]);
				}
				timers[uniqueId] = setTimeout(callback, ms);
			};
		})();

		thisWindow.resize(function () {
			waitForFinalEvent(function() {
				setupTabs();
			}, 50, "tabCheck");
		});

		setupTabs();
		setTimeout(setupTabs(), 10);


		// !------------------------! //
		// !   # AJAX highlights    ! //
		// !------------------------! //
	
		function getHighlights() {

			var 	tabFeedURL,
				thisLang = "",
				isWelsh = true,
				pageLang = "c",
				watchStr = "Gwylio Pennod";

			if ($("html").attr("lang") === "en") {
				isWelsh = false;
				watchStr = "Watch Episode";
			}

			if (!isWelsh) {
				thisLang = "?lang=e";
				pageLang = "e";
			}

			// a hack for this to be used locally
			if (isLocal()) {
				tabFeedURL = "json/test.json";
			} else {
				tabFeedURL = feedURLBase + "/yvd_2/splash_screen" + thisLang;
			}

			$.getJSON(tabFeedURL, function(data) {

				var 	cards = [];
		
				$.each(data.progs_featured, function(i, item) {
					var episode = item.programme_title,
						series = item.series_title;

					if (!series || 0 === series.length) {
						series = episode;
					}

					if (!episode || 0 === episode.length || series === episode) {
						if (item.episode_num > 0) {
							episode = "Episode " + item.episode_num;
						} else {
							episode = "";
						}
					}

					var thisString = '<div class="card watch">' +
										'<div class="card-inner">' +
											'<a href="//s4c.cymru/clic/e_level2.shtml?programme_id=' + item.id + '">' +
												'<img class="lazyload" src="//s4c.cymru/amg/iplayer/' + item.thumbnail_url + '" alt="' + series + '">' +
												'<div class="content">' +
													'<div class="title">' +
														'<i class="material-icons"><svg class="svg-icon"><use xlink:href="svg/symbol-defs.svg#icon-play_circle_outline"></use></svg></i>' +
														'<h2 class="title">' + series + '</h2>' +
														'<span class="sub-title">' + episode + '</span>' +
													'</div>' +
												'</div>' +
											'</a>' +
										'</div>' +
									'</div>';

					cards.push(thisString);
				});
				$("li.highlights-ajax").find(".w2").removeClass("w2");
				$("li.highlights-ajax").prepend(cards.join(""));
			});
		}

		// !------------------------! //
		// !    # AJAX tab genre    ! //
		// !------------------------! //

		function tabContents(genreClass, genreID, maxProgs) {
			// need to pass the genre, this is the class
			// that the tab container has, e.g. drama for Drama
			// the genreID is the id for the genre e.g. drama = 4
			// now get on with it you fucking plum
			var 	tabFeedURL,
				thisLang = "",
				isWelsh = true,
				pageLang = "w",
				watchStr = "Gwylio Pennod",
				thisGenre = "?id=" + genreID,
				i = 1;

			if ($("html").attr("lang") === "en") {
				isWelsh = false;
				watchStr = "Watch Episode";
			}

			if (!isWelsh) {
				thisLang = "&lang=e";
				pageLang = "e";
			}


			// a hack for this to be used locally
			if (isLocal()) {
				tabFeedURL = "json/cats.json";
			} else {
				tabFeedURL = feedURLBase + "/iphone/programme_feed_by_category" + thisGenre + thisLang;
			}

			$.getJSON(tabFeedURL, function(data) {

				var 	cards = [];
		
				$.each(data.progs, function(i, item) {
					if (i >= maxProgs) {
						return false;
					}
					i ++;
					var title = item.series_title,
						subTitle = item.programme_title;

					if (title.length < 1) {
						title = subTitle;
						subTitle = item.hr_txdate;
					}

					var thisString = '<div class="card watch">' +
										'<div class="card-inner">' +
											'<a href="//s4c.cymru/clic/e_level2.shtml?programme_id=' + item.id + '">' +
												'<img class="lazyload" src="//s4c.cymru/amg/iplayer/' + item.thumbnail_url + '" alt="' + title + '">' +
												'<div class="content">' +
													'<div class="title">' +
														'<i class="material-icons"><svg class="svg-icon"><use xlink:href="svg/symbol-defs.svg#icon-play_circle_outline"></use></svg></i>' +
														'<h2 class="title">' + title + '</h2>' +
														'<span class="sub-title">' + subTitle + '</span>' +
													'</div>' +
												'</div>' +
											'</a>' +
										'</div>' +
									'</div>';


					cards.push(thisString);
				});
				$("li." + genreClass).prepend(cards.join(""));
				$("li." + genreClass).find(".w2").removeClass("w2");
			});
		}

		getHighlights();
		tabContents("sport", 5, 10);
		tabContents("drama", 4, 11);
		tabContents("stwnsh", 16, 11);
		tabContents("cyw", 13, 11);
		
		setTimeout(function(){
			setupTabs();
		}, 50);


		// !------------------------! //
		// !  # sgorio news card    ! //
		// !------------------------! //

		function sgorioNews() {

			var sgorioFeedURL,
				sgorioElem = $("div.sgorio-news ul.links"),
				isWelsh = false, 
				thisLang;

			if ($("html").attr("lang") === "en") {
				isWelsh = false;
				thisLang = "e_/";
			}

			// a hack for this to be used locally
			if (isLocal()) {
				sgorioFeedURL = "json/sgorio.json";
			} else {
				sgorioFeedURL = feedURLBase + "/sgorio/" + thisLang + "s4c-penawdau-json";
			}

			$.getJSON(sgorioFeedURL, function(data) {

				var thisHTML = [],
					thisString;

				$.each(data, function(i, item) {


					if (i >= 4) {
						return false;
					}

					i ++;

					thisString = '<li>' +
									'<a href="' + item.link + '">' +
										'<img src="' + item.image + '" alt="' + item.title + '">' +
										'<h3>' + item.title + '</h3>' +
									'</a>' +
								'</li>';

					thisHTML.push(thisString);
				});

				sgorioElem.html(thisHTML.join(""));

			});
		}

		sgorioNews();

	};






	// !------------------------! //
	// !     # large slider     ! //
	// !------------------------! //

	$.fn.acLargeSlider = function() {

		// Custom plugin for large slider
		// we are expecting to be passed
		// a containing div with a list
		// ul.s4c-large-slider-container with
		// > li.s4c-large-slide being
		// individual slides therin

		// need to throw a pause function in here someplace


		var thisWindow = $(window),
			windowWidth,
			windowHeight,
			elem = $(this),
			elemList = elem.find("ul.s4c-large-slider-container"),
			slideCount = elemList.children("li").size(),
			listWidth,
			currentSlide = 0,
			pauseDur = 300,
			animateDur = 600,
			animating = false,
			slideFrequency = 8000,
			timer,
			paused = false,
			elemControls = elem.find("div.s4c-large-slider-controls"),
			bigSwipeElem = elem.find("div.big-swipe");


		timer = $.timer(function() {
			animateNext();
		});


		timer.set({ time : slideFrequency, autostart : true });


		function sortPips()  {

			var onClass = "",
				i;

			for (i = 0; i < slideCount; i++) {
				if (i === 0) {
					onClass = "on";
				} else {
					onClass = "";
				}
				$("ul.s4c-pips").append('<li data-slide="' + i + '" class="s4c-pip ' + onClass + '">	<a href="#" class="btn pip"></a></li>');
			}
		}

		function sortTabIndex() {
			elemList.children("li").find("a").attr("tabindex","-1");
			elemList.children("li").eq(currentSlide).find("a").removeAttr("tabindex");
		}

		function setupSlider() {
			windowWidth = thisWindow.width();
			windowHeight = thisWindow.height();
			listWidth = slideCount * windowWidth;
			if (windowHeight > 768) {
				windowHeight = windowHeight * 0.75;
			} else if (windowHeight < 400) {
				windowHeight = 400;
			} else {
				windowHeight = windowHeight * 0.9;
			}
			elem.height(windowHeight);
			elemList
				.width(listWidth)
				.css({left: (currentSlide * windowWidth) * -1})
				.children("li")
					.width(windowWidth);

			sortTabIndex();
		}

		elem.find("div.slide-content p:first-of-type").succinct({
			 size: 200
		});

		// initialise this fucking bastard
		sortPips();
		setupSlider();

		// resize control
		var waitForFinalEvent = (function () {
			var timers = {};
			return function (callback, ms, uniqueId) {
				if (!uniqueId) {
					uniqueId = "Needs a unique ID";
				}
				if (timers[uniqueId]) {
					clearTimeout (timers[uniqueId]);
				}
				timers[uniqueId] = setTimeout(callback, ms);
			};
		})();

		thisWindow.resize(function () {
			waitForFinalEvent(function() {
				// just getting window width
				windowWidth = thisWindow.width();
				setupSlider();
			}, 50, "resizecheck");
		});


		function animateSlide(direction) {

			animating = true;
			timer.stop();

			bigSwipeElem.addClass(direction);

			elemList	
				.addClass(direction)
				.delay((pauseDur-200))
				.animate({
					left: "-" + (currentSlide * windowWidth)
				}, (animateDur+200), function() {

					setTimeout(function(){

						// resest state of animating to false now
						// that this callback confirms that's the case your honour
						// plus I've added another delay so it matches
						// the length of the CSS animation your honour
						elemList.removeClass(direction);

					}, pauseDur);
				});

			setTimeout(function(){
				animating = false;
				if (!paused) {
					timer.play();
				}
				bigSwipeElem.removeClass(direction);
			}, 1200);

			// sort out da pips ya cunt
			$("ul.s4c-pips li.s4c-pip").removeClass("on");
			$("ul.s4c-pips li.s4c-pip[data-slide='" + currentSlide + "']").addClass("on");
			// then sort out the tabindex you dick
			sortTabIndex();
		}


		function animateNext() {
			var direction = "left"; // direction of travel for slides

			if (!animating) {
				// only do this if we're not currently animating
				currentSlide ++;

				if (currentSlide >= slideCount) {
					// Reached the end
					// Start again mother fucker
					currentSlide = 0;
					direction = "right";
				}
				// animate the heck outta this
				animateSlide(direction);
			}
		}

		function animatePrevious() {
			var direction = "right"; // direction of travel for slides

			if (!animating) {
				// only do this if we're not currently animating
				currentSlide --;

				if (currentSlide < 0) {
					// Reached the beginning
					// Start again at t'other end
					currentSlide = (slideCount - 1);
					direction = "left";
				}
				// animate the heck outta this
				animateSlide(direction);
			}
		}

		// NEXT has been clicked
		$("a.control.next").on("click", function(e){
			e.preventDefault();
			// remove focus when clicked
//			$(this).blur();
			animateNext();
		});

		// PAUSE has been clicked
		elemControls.on("click", "a.pause", function(e){
			e.preventDefault();
			if (paused) {
				$(this).removeClass("paused");
				timer.play();
				paused = false;
			} else {
				$(this).addClass("paused");
				timer.stop();
				paused = true;
			}
		});


		// PREVIOUS has been clicked
		$("a.control.previous").on("click", function(e){
			e.preventDefault();
			// remove focus when clicked
			// actually needs more thought.
			// if focus was given with a keystroke then
			// it should remain
			// will have to have a think on this
//			$(this).blur();

			animatePrevious();
		});


		// Arrow key navigation (left + right)
		$(document).keydown(function(e) {
			switch(e.which) {

				case 37: // left
					animatePrevious();
				break;

				case 39: // right
					animateNext();
				break;

				default: return; // exit this handler for other keys
			}
			e.preventDefault(); // prevent the default action (scroll / move caret)
		});



		$(function() {
			//Enable swiping...
			$("ul.s4c-large-slider-container").swipe({
				//Generic swipe handler for all directions
				swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
					if(direction === "right") {
						animatePrevious();
					} else {
						animateNext();
					}
				}
			});
			$("ul.s4c-large-slider-container").swipe({
				allowPageScroll: "auto"
			});
		});

		// A PIP has been clicked
		$("li.s4c-pip a.pip").on("click", function(e){
			e.preventDefault();
			// remove focus when clicked
			$(this).blur();

			if (!animating) {

				var elem = $(this),	
					elemParent = elem.parent("li"),
					thisSlide = elemParent.attr("data-slide"),
					direction;

				if (currentSlide !== thisSlide) {

					if (currentSlide > thisSlide) {
						direction = "right";
					} else {
						direction = "left";
					}

					currentSlide = thisSlide;
					// animate the heck outta this
					animateSlide(direction);
	
				}
			}
		});


		// pause slider if mobile nav is open
		$("a.s4c-toggle-btn").on("click", function(e){
			if (!paused) {

				if ($("body").hasClass("show-nav")) {
					timer.play();
				} else {
					timer.stop();
				}
			}
		});
		// play it if overlay is pressed and
		// not currently paused.
		$("div.s4c-main-overlay").on("click", function(){
			if (!paused) {
				timer.play();
			}
		});


	};

})(this.jQuery);


var 	thisDocument = $(document),
	scrollPosition = thisDocument.scrollTop(),
	returnPosition,
	logoReturn,
	thisWindow = $(window),
	windowWidth = thisWindow.width(),
	windowHeight = thisWindow.height(),
	headerElem = $("header.s4c-header"),
	mobileNavWidth = 1074;


// !------------------------! //
// !      # Language        ! //
// !------------------------! //

var isWelsh = true;

if ($("html").attr("lang") === "en") {
	isWelsh = false;
}

// or do I put this in a function?

function iWelsh() {
	if ($("html").attr("lang") === "en") {
		return false;
	} else {
		return true;
	}
}

var feedURLBase =  window.location.protocol + "//" + window.location.host;

function isLocal() {
	// am I running this locally?
	// effects cross domain bollucks is all
	if (feedURLBase === "http://webmb01.local:5757"  || feedURLBase === "http://webmb01.s4c.co.uk:5757/" || feedURLBase === "http://localhost:5757" || feedURLBase === "http://localhost:5757/") {
		return true;
	} else {
		return false;
	}
}

// !------------------------! //
// !   # Resize Control     ! //
// !------------------------! //

// when resizing, need to get the
// window width and maybe fire
// off some other functions, so 
// introducing a delay. That
// resize is a hungry resource bitch
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "Needs a unique ID";
		}
		if (timers[uniqueId]) {
			clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();


// !------------------------! //
// !     # Sticky nav       ! //
// !------------------------! //

// variables only used by sicky nav section
var navElement = $("header.s4c-header"),
	toggleElement = $("div.s4c-toggle-menu"),
	s4cLogo = $("a.s4c-logo-toggle"),
	isStuck = false,
	isStuckBtn = false,
	menuToggle = $("a.s4c-toggle-btn"),
	navHeight,
	bodyElem = $("body"),
	adBanner = $("div.bannerAd");

function stickyNav(x) {
	if (x === 1) {
	}
	if (windowWidth > 1074) {
		// largest navigation - do that stuff then
		navHeight = navElement.height() + adBanner.outerHeight(true);

		if (navHeight <= scrollPosition) {
			// if the header is OUT of view
			// then add 'sticky' class
			if (!isStuck) {
				navElement.addClass("sticky");
				s4cLogo.addClass("sticky");
				isStuck = true;
//					bodyElem.css({paddingTop:navHeight});
			}
		} else {
			// if the header is IN view
			// then remove 'sticky' class
			if (isStuck) {
				navElement.removeClass("sticky");
				s4cLogo.removeClass("sticky");
				isStuck = false;
//					bodyElem.css({paddingTop:0});
			}
		}

	} else {
		// mobile navigation - do this stuff instead
//			navHeight = toggleElement.height() + adBanner.outerHeight(true);
		navHeight = adBanner.outerHeight(true);
		s4cLogo.css({marginTop: adBanner.outerHeight(true)});
		toggleElement.css({top: adBanner.outerHeight(true)});

		if (navHeight <= scrollPosition) {
			// if  the header is OUT of view
			// then add 'sticky' class 
			toggleElement.css({top: 0}).addClass("sticky");
			if (!isStuckBtn) {
				toggleElement.css({top: 0}).addClass("sticky");
//				s4cLogo.addClass("sticky");
				isStuckBtn = true;
			}
		} else {
			// if the header is IN view
			// then remove 'sticky' class
			if (isStuckBtn) {
				toggleElement.css({top: adBanner.outerHeight(true)}).removeClass("sticky");
//				s4cLogo.removeClass("sticky");
				isStuckBtn = false;
			}
		}
	}
}


// !------------------------! //
// !     # Toggle nav       ! //
// !------------------------! //

$("a.s4c-toggle-btn").on("click", function(e){
	e.preventDefault();
	// switch between hiding and
	// showing the nav
	var bodyElem = $("body"),
		adBanner = $("div.bannerAd"),
		accurateScrollPosition = thisDocument.scrollTop(), // required because the delay on my global scrolling function was causing a jump
		logoElem = $("a.s4c-logo-toggle"),
		logoPosition = logoElem.offset().top - parseInt(logoElem.css("margin-top")),
		logoShowPosition = logoPosition - accurateScrollPosition;

	// remove focus when clicked
//		$(this).blur();

	function showNav() {
		var adHeight = adBanner.outerHeight(true),
			adDifference = adHeight - accurateScrollPosition;


		if (adDifference < 0) {
			adDifference = 0;
		}

		bodyElem.addClass("show-nav");
		// Controlling the scroll position
		// of the frozen content
		returnPosition = accurateScrollPosition;
		logoReturn = logoPosition;

		$("body.show-nav div.s4c-main-container")
			.height(windowHeight - adDifference)
			.css({top: adDifference})
			.scrollTop(accurateScrollPosition - adHeight);


		logoElem.css({top: logoShowPosition});


	}

	function hideNav() {
		bodyElem.removeClass("show-nav");
		// Controlling the scroll position
		// of window on returne
		$(document).scrollTop(returnPosition);
		logoElem.css({top: logoReturn});
	}

	if ($("body").hasClass("show-nav")) {
		hideNav();
	} else {
		showNav();
	}
	
	// if div.s4c-main-overlay
	// is clicked, close dat nav shit
	$("div.s4c-main-overlay").on("click", function(){
		hideNav();
	});
});


// !------------------------! //
// !   # Playing with BGs   ! //
// !------------------------! //

function parallaxBG() {

	if (windowWidth > 810) {

		var pFactor = 0.5,
			pAdjust = scrollPosition * pFactor,
			scrollPercent = (100 / windowHeight) * scrollPosition,
			oMin = 0.2,
			oOpacity = (100 - (scrollPercent * 2)) / 100,
			adBanner = $("div.advert-banner");

		// offest background image inine with banner height
		$("div.image-background"	)
			.css({
				transform: "matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, " + (pAdjust - adBanner.height()) + ", 0, 1)",
				paddingBottom: adBanner.height()
			});

		if (oOpacity < oMin) {
			// bottom base level for opacity
			oOpacity = oMin;
		}

		// move background and make fadeout/in
		$("div.image-background"	)
			.css({
				transform: "matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, " + (pAdjust - adBanner.height()) + ", 0, 1)",
				"opacity": oOpacity
			});

	} else {
		// not interested in parallax at this width
		// reset any css I may have forced above
		$("div.image-background"	)
			.css({
				transform: "matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)",
				"opacity": 1
			});

	}
}




// !------------------------! //
// !  # Handle scroll stuff ! //
// !------------------------! //


function scrollHandler() {
	// manage scroll event here
	scrollPosition = thisDocument.scrollTop();
	stickyNav();
	parallaxBG();

}

$( window ).scroll(function() {
	// hand off all management of scrollevent
	window.requestAnimationFrame(scrollHandler);
});


// !------------------------! //
// !  # Initialise page     ! //
// !------------------------! //

scrollHandler(); // firing this because it contains everything we need for page setup


// !------------------------! //
// !    # context menu      ! //
// !------------------------! //

var mainNavElem = $("ul.s4c-main-menu"),
	dropMenuElem = $("div.context-menu ul"),
	navNowNext = $("div.nav-now-next");


function closeS4CSubMenu(thisElem) {
	thisElem
		.removeClass("show")
		.addClass("hide")
		.siblings("div.context-menu")
			.addClass("hide")
			.find("a")
				.attr("tabindex","-1");
	setTimeout(function(){
		thisElem.siblings("div.context-menu").css({display:"none"});
	}, 300);
	if (thisElem.hasClass("s4c-menu-item-link")) {
		// it's the primary nav, sort the margin
		// for the mobile nav
		thisElem.parent("li").removeClass("show");
	}
	navNowNext.removeClass("hide");
}

function openS4CSubMenu(thisElem) {
	thisElem.siblings("div.context-menu").css({display:"block"});
	setTimeout(function(){
		thisElem
			.addClass("show")
			.removeClass("hide")
			.siblings("div.context-menu")
				.removeClass("hide")
				.find("a")
					.removeAttr("tabindex");
	}, 50);

	if (thisElem.hasClass("s4c-menu-item-link")) {
		// it's the primary nav, sort the margin
		// for the mobile nav
		thisElem.parent("li").addClass("show");
	}
	navNowNext.addClass("hide");
}


// behaviour when clicking
mainNavElem.on("click", "a.s4c-drop-down-nav.hide", function(e){
	e.preventDefault();
	openS4CSubMenu($(this));
});

mainNavElem.on("click", "a.s4c-drop-down-nav.show", function(e){
	e.preventDefault();
	closeS4CSubMenu($(this));
//		$(this).blur();
});

// check focus
function checkDropFocus() {
	if (dropMenuElem.find("a:focus").size() === 0) {
		return false;
	} else {
		return true;
	}
}

// check focus on blur
dropMenuElem.find("a").blur(function(){
	var dropLink = $(this).closest("div.context-menu").siblings("a.s4c-drop-down-nav");
	setTimeout(function(){
		if (!checkDropFocus()) {
			closeS4CSubMenu(dropLink);
		}
	}, 200);
});

// on blur
mainNavElem.on("blur", "a.s4c-drop-down-nav.show", function(e){
	var dropLink = $(this);
	setTimeout(function(){
		if (!checkDropFocus()) {
			closeS4CSubMenu(dropLink);
		}
	}, 200);
});


// !------------------------! //
// !  # Homepage listings   ! //
// !------------------------! //

var thisURLBase = window.location.protocol + "//" + window.location.host,
	isOpen = false,
	d = new Date(),
	currHour = d.getHours(),
	currMin = d.getMinutes(),
	currTotalMins,
	listingsElem = $("#listings-slider"),
	programmesElem = $("#listings-slider div.programmes"),
	timeElem = $("#listings-slider span.time"),
	hourWidth = 320,
	listingsCurrentTime,
	entityMap = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};

currTotalMins = ( currHour * 60 ) + currMin;

function sortCurrentTime() {
	programmesElem.find("div.programme").each(function(){

		var thisElem = $(this),
			startMins = parseInt(thisElem.attr("data-startMins")),
			endMins = parseInt(thisElem.attr("data-endMins"))
			;

		if (currTotalMins >= startMins && currTotalMins <= endMins ) {

			var currentTimeElem = $("div.current"),
				currentElem = $(this);

			listingsCurrentTime = currentElem.position().left + (((currTotalMins - startMins) * (100 / 60)) * (100 / hourWidth));

			currentTimeElem.animate({width:listingsCurrentTime});
		}

	});
	centreListings();
}


function centreListings() {
	// put the current time in the middle or as close as possible
	// current position is
	var timePosition = listingsCurrentTime,
		offSetHalf = windowWidth / 2,
		maximumScroll = programmesElem.find("div.programme:last").offset().left + programmesElem.find("div.programme:last").width();

	timePosition = timePosition - offSetHalf; // this centres the time rather then being far left all Corbyn-like

	// if it's trying to scroll minus style then this...
	if (timePosition < 0) {
		timePosition = 0;
	}

	// what about maximum range?
	if (timePosition > maximumScroll) {
		timePosition = maximumScroll;
	}

	$("div#listings-slider div.listings").scrollLeft(timePosition);
}


function truncateTxt(txt, maxLen) {
	var len = txt.length;

	if (len > maxLen) {
		return txt.substr(0,maxLen) + '...';
	} else {
		return txt;
	}

}


function escapeHtml(string) {
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
	});
}

function getMinutes(x) {
	// x should be passed as hh:mm:ss
	var hMins,
		mMins,
		sMins,
		totalMins,
		timeParts;

	timeParts = x.split(":");
	hMins = parseInt(timeParts[0]) * 60;
	mMins = parseInt(timeParts[1]);
	sMins = parseInt(timeParts[2]) / 60;
	totalMins = hMins + mMins + sMins;

	return totalMins;
}

function tidyTime(x) {
	// x should be passed as hh:mm:ss
	var hours,
		mins,
		timeParts;

	timeParts = x.split(":");
	hours = timeParts[0];
	mins = timeParts[1];

	return hours + ":" + mins;
}

var thisLang = "",
	feedURL = "";

if (!isWelsh) {
	thisLang = "?lang=e";
}

// a hack for this to be used locally
if (thisURLBase === "http://webmb01.local:5757"  || thisURLBase === "http://webmb01.s4c.co.uk:5757/" || thisURLBase === "http://localhost:5757" || thisURLBase === "http://localhost:5757/") {
	feedURL = "feed/listings.json";
} else {
	feedURL = thisURLBase + "/iphone/listings_today" + thisLang;
}

$.getJSON(feedURL, function(data) {

	var latestEnd = 0,
		totalSize = data.today.length,
		myCount = 0;

	$.each(data.today, function(i, item) {
		// Variables. OBVIOUSLY!
		// more local scope than that loner above
		var startMins,
			startTime,
			endMins,
			endTime,
			durMins,
			universalStartMins,
			thisHTML,
			thisTitle,
			startTidy,
			endTidy,
			isCatchup,
			setReminderStr,
			watchStr,
			enCalStr;

		isCatchup = false;
		startTime = item.date_broadcast.split(" ");
		endTime = item.end_dt.split(" ");
		thisTitle = item.series_title;
		setReminderStr = "Set reminder";
		watchStr = "Watch";
		enCalStr = "&en=1";

		if (!thisTitle) {
			thisTitle = item.programme_title;
		}

		if (isWelsh) {
			setReminderStr = "Ychwanegu at y Calendr";
			watchStr = "Gwylio";
			enCalStr = "";
		}

		startMins =  getMinutes(startTime[1].split("+")[0]);
		durMins = getMinutes(item.duration);
		endMins = startMins + durMins;
		startTidy = tidyTime(startTime[1].split("+")[0]);
		endTidy = tidyTime(endTime[1].split("+")[0]);

		if (item.mp4 && item.mp4 !== "null") {
			isCatchup = true;
		}

		if (endMins > latestEnd ) {

			thisHTML = "<div class='programme' data-durMins='" + durMins + "' data-startMins='" + startMins + "' data-endMins='" + endMins + "' data-index= '" + myCount + "' data-watch='" + isCatchup + "' style='left:" + (hourWidth * myCount) + "px; width:" + hourWidth + "px'>" +
							"<h3>" + escapeHtml(thisTitle) + "</h3>" +
							"<span>" + startTidy + " – " + endTidy + "</span>" +
							"<div class='programme-content'>" +
								"<img class='lazyload' src='//s4c.cymru/amg/web/640x360/" + item.thumbnail_url + "' alt='" + escapeHtml(thisTitle) + "'>" +
								"<p>" + truncateTxt(escapeHtml(item.short_billing), 150) + "</p>" +
								"<div class='actions'>" +
									"<a class='watch' href='http://s4c.cymru/clic/e_level2.shtml?programme_id=" + item.id + "'>" +
										"<i class='material-icons'><svg class='svg-icon'><use xlink:href='svg/symbol-defs.svg#icon-play_circle_outline'></use></svg></i>" +
										" + watchStr + " +
									"</a>" +
									"<a class='reminder' href='http://s4c.co.uk/ical_script.php?pid=" + item.id + enCalStr + "'>" +
										"<i class='material-icons'><svg class='svg-icon'><use xlink:href='svg/symbol-defs.svg#icon-event_available'></use></svg></i>" +
										" + setReminderStr + " +
									"</a>" +
								"</div>" +
							"</div>" +
						"</div>";

			programmesElem.append(thisHTML);
			latestEnd = endMins;
			myCount++;

		}

		if (i === totalSize - 1) {
			// this is the last one
			setTimeout(function(){
				sortCurrentTime();
				centreListings();
			}, 200);
		}

	});

});


function expandSrhink() {
	if (isOpen) {

		listingsElem.removeClass("open");
		isOpen = false;

	} else {

		listingsElem.addClass("open");
		isOpen = true;

	}
}



// click control

// open
listingsElem.on("click", "div.programme", function(e) {
	e.preventDefault();

	var parentOffset = $(this).offset(),
		relY = e.pageY - parentOffset.top;

	if (relY <= 80) {
		expandSrhink();
	}

});


// toggle
listingsElem.on("click", "a.toggle", function(e) {
	e.preventDefault();
	expandSrhink();
});


// !------------------------! //
// !   # Corporate links    ! //
// !------------------------! //

// accordian control
// grab click for h3
$("div.corporate").on("click", "div.links > h3", function(){
	if (windowWidth < 670) {
		var thisElem = $(this);

		if (!thisElem.hasClass("show")) {
			$("div.corporate div.links h3.show")
				.removeClass("show")
				.siblings("div.links-content")
				.slideUp();
			thisElem
				.addClass("show")
				.siblings("div.links-content")
				.slideDown();
		} else {
			thisElem
				.removeClass("show")
				.siblings("div.links-content")
				.slideUp();
		}
	}
});

// if over certain width, need to use
// javascript to make sure heights
// are the same
function corporateLinks(){
	var linksElem = $("div.corporate").find("div.links:not(.w2)");
		linksElem.height("auto");
	if (windowWidth > 1329) {
		var highest = 0;
		linksElem.each(function(){
			var thisHeight = $(this).height();
			if (thisHeight > highest) {
				highest = thisHeight;
			}
		});
		linksElem.height(highest);
	} else {

		linksElem.height("auto");
	}
}

corporateLinks();


// !------------------------! //
// ! # AJAX corporate links ! //
// !------------------------! //

function wordpressLinks(feedURL, maxLinks, catClass) {

	var thisElem = $("div." + catClass + " ul");

	$.get(feedURL, function(data) {

		var $xml = $(data),
			thisHTML = [],
			thisString;

		$xml.find("item").each(function(i) {

			var thisItem = $(this),

				item = {
					title: thisItem.find("title").text(),
					link: thisItem.find("link").text()
				}

			
			if (i >= maxLinks) {
				return false;
			}

			i ++;

			thisString = '<li>' +
							'<a href="' + item.link + '">' + item.title +
						 '</li>';

			thisHTML.push(thisString);

		});

		thisElem.html(thisHTML.join(""));
		setTimeout(corporateLinks(), 100)

	});

}

if (isWelsh) {

	wordpressLinks("/caban/?cat=9&feed=rss2", 3, "features");
	wordpressLinks("/caban/?cat=16&feed=rss2", 3, "competitions");
	wordpressLinks("/caban/?cat=19&feed=rss2", 3, "events");
	wordpressLinks("/caban/?cat=7&feed=rss2", 3, "be-on-tv");

} else {

	wordpressLinks("/caban/?cat=8&lang=en&feed=rss2", 3, "features");
	wordpressLinks("/caban/?cat=17&lang=en&feed=rss2", 3, "competitions");
	wordpressLinks("/caban/?cat=20&lang=en&feed=rss2", 3, "events");
	wordpressLinks("/caban/?cat=6&lang=en&feed=rss2", 3, "be-on-tv");

}



// !------------------------! //
// ! # AJAX corporate links ! //
// !------------------------! //

function pressLinks(feedURL, maxLinks, catClass) {

	var thisElem = $("div." + catClass + " ul");

	$.get(feedURL, function(data) {

		var $xml = $(data),
			thisHTML = [],
			thisString;

		$xml.find("entry").each(function(i) {

			var thisItem = $(this),

				item = {
					title: thisItem.find("title").text(),
					link: thisItem.find("link").text()
				}

			
			if (i >= maxLinks) {
				return false;
			}

			i ++;

			thisString = '<li>' +
							'<a href="' + item.link + '">' + item.title +
						 '</li>';

			thisHTML.push(thisString);

		});

		thisElem.html(thisHTML.join(""));
		setTimeout(corporateLinks(), 100)

	});

}

if (isWelsh) {

	pressLinks("/cymraeg_s4c_atom_news_feed.xml", 3, "press");

} else {

	pressLinks("/english_s4c_atom_news_feed.xml", 3, "press");

}


// !------------------------! //
// !   # mobile now/next    ! //
// !------------------------! //

function mobileNowNext() {

	var nowNextFeedURL,
		nowNextElem = $("#s4c-mobile-now-next"),
		liveArr = ["Live","Yn fyw"],
		nextArr = ["On next", "Ar nesaf"],
		isWelsh = false, 
		thisLang;

	if ($("html").attr("lang") === "en") {
		isWelsh = false;
		thisLang = "?lang=e";
	}

	// a hack for this to be used locally
	if (isLocal()) {
		nowNextFeedURL = "json/now-next.json";
	} else {
		nowNextFeedURL = feedURLBase + "/oracle/now_and_next" + thisLang;
	}

	$.getJSON(nowNextFeedURL, function(data) {

		var thisHTML = [],
			thisString;

		$.each(data.now_and_next, function(i, item) {

			var labelStr,
				title = item.series_title,
				lang = "e";

			if (isWelsh) {
				lang = "c";
			}
	
				if (i === 0) {
				// live
				if (isWelsh) {
					labelStr = liveArr[1];
				} else {
					labelStr = liveArr[0];
				}
			} else {
				// on next
				if (isWelsh) {
					labelStr = nextArr[1];
				} else {
					labelStr = nextArr[0];
				}
			}


			i ++;

			if (title.length < 1) {
				title = item.programme_title;
			}

			thisString = '<div class="now">' +
							'<a href="//s4c.cymru/clic/' + lang + '_live.shtml">' +
								'<img src="//s4c.cymru/amg/web/122x72/' + item.thumbnail_url + '"' + labelStr + '">' +
								'<h3>' + labelStr + '</h3>' +
								'<h4>' + title + '</h4>' +
							'</a>' +
						'</div>';


			thisHTML.push(thisString);
		});

		nowNextElem.prepend(thisHTML.join(""));


	});
}

mobileNowNext();

// !------------------------! //
// !   # now/next    		! //
// !------------------------! //

var nowNextFeedURL;

// a hack for this to be used locally
if (isLocal()) {
    nowNextFeedURL = "json/now-next.json";
} else {
    nowNextFeedURL = feedURLBase + "/oracle/now_and_next?feed=json";
}

var nowData;

function nowNext() {

	var nowNextElem = $(".now-next .content"),
	    liveArr = ["Watch live","Yn fyw", "now"],
	    nextArr = ["Watch live","Yn fyw", "next"],
	    isWelsh = false, 
	    thisLang;

	if ($("html").attr("lang") === "en") {
	    isWelsh = false;
	    thisLang = "?lang=e";
	}

	$.getJSON(nowNextFeedURL, function(data) {

	    var thisHTML = [],
	        thisString,
	        d = data;
	       

	    $.each(data.now_and_next, function(i, item) {

	        var labelStr,
	        	nowNext,
	            title 			= item.series_title,
	            programme_title = item.programme_title,
	            lang 			= "e",
	            // cutoff date, get time string - all time vars just needs to be 'now' data?
	            startTime 		= item.date_broadcast.slice(11,-6),
	            endTime 		= item.end_dt.slice(11,-6),
	            // convert time to integer, get hour and minute
	            startHour		= parseInt(startTime.slice(0, 2)),
	            endHour			= parseInt(endTime.slice(0, 2)),
	            // convert minutes to decimals
	            startMinute		= parseInt(startTime.slice(-2)) / 60,
	            endMinute		= parseInt(endTime.slice(-2)) / 60,
	        	start 			= startHour + startMinute,
	            end 			= endHour + endMinute,
	            duration		= end - start,
	            timeNow 	    = (new Date().getHours() + (new Date().getMinutes() / 60)) * 10 / 10,
	            returnTime 		= item.return_time;

	        if (isWelsh) {
	            lang = "c";
	        }

	        if (i === 0) {
	            // live
	            if (isWelsh) {
	                labelStr = liveArr[1];
	                nowNext  = liveArr[2];
	            } else {
	                labelStr = liveArr[0];
	                nowNext  = liveArr[2];
	            }
	            // compare difference between start and timeNow to duration to calculate percentage complete of live
	            progress 	 = ((timeNow - start) / duration) * 100 + '%';
	        } else {
	            // on next
	            if (isWelsh) {
	                labelStr = nextArr[1];
	                nowNext  = nextArr[2];
	            } else {
	                labelStr = nextArr[0];
	                nowNext  = nextArr[2];
	            }
	        }


	        i ++;

	        if (title.length < 1) {
	            title = item.programme_title;
	        }

	        thisString = '<div class="' + nowNext + '-wrapper" style="background-image: url(//s4c.cymru/amg/iplayer/' + item.thumbnail_url + ')">' +
	            			'<div class=' + nowNext + '>' +	
		                        '<a href="//s4c.cymru/clic/' + lang + '_live.shtml">' +
		                        	'<i class="material-icons"><svg class="svg-icon"><use xlink:href="svg/symbol-defs.svg#icon-play_circle_outline"></use></svg></i>' + labelStr + 
		                        '</a>' +
		                        '<h2><span class="series-title">' + title +  '</span><span class="programme-title"> - ' + programme_title + '</span></h2>' +
		                        '<div class="progress">' +
		                        	'<div class="progress-overlay" style="width: ' + progress + '"></div>' +
		                        '</div>' +
		                        '<p><span class="startTime">' + startTime + '</span> - <span class="endTime">' + endTime + '</span></p>' +
	                        '</div>' +
	                     '</div>';


	        thisHTML.push(thisString);

	    });

	    nowNextElem.prepend(thisHTML.join(""));

	    // Hide programme title if not given
	    if($('.programme-title').html() === " - null"){
	    	$('.programme-title').hide();
	    }
	    
	    // Now data is accessible globally
	    return nowData = d.now_and_next[0];
    });
}

nowNext();

// update progress bar every minute
setInterval(function(){
	$.getJSON(nowNextFeedURL, function(data) {
        // cut off date, get time string
        var now 			= data.now_and_next[0],
        	startTime 		= now.date_broadcast.slice(11,-6),
            endTime 		= now.end_dt.slice(11,-6),
            // convert time to integer, get hour and minute
            startHour		= parseInt(startTime.slice(0, 2)),
            endHour			= parseInt(endTime.slice(0, 2)),
            // convert minutes to decimals
            startMinute		= parseInt(startTime.slice(-2)) / 60,
            endMinute		= parseInt(endTime.slice(-2)) / 60,
        	start 			= startHour + startMinute,
            end 			= endHour + endMinute,
            duration		= end - start,
            timeNow 	    = (new Date().getHours() + (new Date().getMinutes() / 60)) * 10 / 10,
            returnTime 		= item.return_time;
            // compare difference between start and timeNow to duration to calculate percentage complete of live
            progress 	    = ((timeNow - start) / duration) * 100;
        	$('.progress-overlay').css('width', progress + '%');

        	// if programme finished fade out, fade in next
        	if(progress >= 100){ 
        		// if(returnTime < 0){}
        		// now slides out...
        		// $('.now-wrapper').addClass('fadeOut');
        		setTimeout(function(){
        			// next slides in, progress bar is 0%
        			// $('.next-wrapper').addClass('fadeIn');
        		}, 500);
        		
        		// update now with next data (setTimeout) - needs refactor, not DRY
                now 			= data.now_and_next[1],
            	startTime 		= now.date_broadcast.slice(11,-6),
                endTime 		= now.end_dt.slice(11,-6),
                // convert time to integer, get hour and minute
                startHour		= parseInt(startTime.slice(0, 2)),
                endHour			= parseInt(endTime.slice(0, 2)),
                // convert minutes to decimals
                startMinute		= parseInt(startTime.slice(-2)) / 60,
                endMinute		= parseInt(endTime.slice(-2)) / 60,
            	start 			= startHour + startMinute,
                end 			= endHour + endMinute,
                duration		= end - start,
                timeNow 	    = (new Date().getHours() + (new Date().getMinutes() / 60)) * 10 / 10,
                returnTime 		= item.return_time;
                progress 	    = ((timeNow - start) / duration) * 100;
                // wait 10 secs then update now with next data..
        		$('.now .series-title').text(now.series_title);
        		$('.now-wrapper').css('background-image', 'url(//s4c.cymru/amg/iplayer/' + now.thumbnail_url + ')');
        		$('.now .startTime').text(now.date_broadcast.slice(11,-6));
        		$('.now .endTime').text(now.end_dt.slice(11,-6));
        		$('.progress-overlay').css('width', progress + '%');
        	}
	});
// update progress bar every minute
}, 60000);

function getTime(){
	// include ajax call here
	$.getJSON(nowNextFeedURL, function(data) {
			// title 			= data.series_title,
			// programme_title  = data.programme_title,
		 	// lang 			= "e",
		
			// cutoff date, get time String
		var now 			= data.now_and_next[1],
			startTime 		= now.date_broadcast.slice(11,-6),
		    endTime 		= now.end_dt.slice(11,-6),
		    // convert time to integer, get hour and minute
		    startHour		= parseInt(startTime.slice(0, 2)),
		    endHour			= parseInt(endTime.slice(0, 2)),
		    // convert minutes to decimals
		    startMinute		= parseInt(startTime.slice(-2)) / 60,
		    endMinute		= parseInt(endTime.slice(-2)) / 60,
			start 			= startHour + startMinute,
		    end 			= endHour + endMinute,
		    duration		= end - start,
		    timeNow 	    = (new Date().getHours() + (new Date().getMinutes() / 60)) * 10 / 10,
		    progress 	    = ((timeNow - start) / duration) * 100;
	});
};



// !------------------------! //
// !    # Resize Actions    ! //
// !------------------------! //

thisWindow.resize(function () {
	waitForFinalEvent(function() {
		// just getting window width
		// for now
		windowWidth = thisWindow.width();
		// when screen is resized, we
		// need to re-fire stickyNav
		stickyNav();
		// and look at parrallax thing
		parallaxBG();
		// and check corporate links
		corporateLinks();
//			checkListingsSize();
	}, 50, "resizecheck");
});
























