

// !------------------------! //
// !      # Advertising     ! //
// !------------------------! //

var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') + 
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
})();



googletag.cmd.push(function() {

	var lang = $("div.bannerAd").attr("data-lang");

	var mapping = googletag.sizeMapping().
		addSize([320, 200], [320, 50]).
		addSize([768, 200], [728, 90]).
		addSize([1000, 200], [[970, 90],[728, 90]]).
		build();
	var mapping2 = googletag.sizeMapping().
		addSize([300, 250], [300, 250]).
		build();

	if (lang === "cy") {
		window.slot1 = googletag.defineSlot( '/20346936/s4c/s4c_homepage', [320, 50], 'div-gpt-ad-1418129986259-0').
		defineSizeMapping(mapping).
		addService(googletag.pubads()).
		setCollapseEmptyDiv(true);
		
		window.slot2 = googletag.defineSlot( '/20346936/s4c/s4c_homepage', [300, 250], 'div-gpt-ad-1418129986259-1').
		defineSizeMapping(mapping2).
		addService(googletag.pubads()).
		setCollapseEmptyDiv(true);
	
	} else {
		window.slot1 = googletag.defineSlot( '/20346936/s4c/s4c_english', [320, 50], 'div-gpt-ad-1421334354557-0').
		defineSizeMapping(mapping).
		addService(googletag.pubads()).
		setCollapseEmptyDiv(true);
		
		window.slot2 = googletag.defineSlot( '/20346936/s4c/s4c_english', [300, 250], 'div-gpt-ad-1421334354557-1').
		defineSizeMapping(mapping2).
		addService(googletag.pubads()).
		setCollapseEmptyDiv(true);
	
	}

	googletag.pubads().enableSingleRequest();
	googletag.enableServices();
	googletag.pubads().addEventListener('slotRenderEnded', function(event) {
		console.log('Creative with id: ' + event.creativeId + ' is rendered to slot of size: ' + event.size[0] + 'x' + event.size[1]);
		stickyNav(1);
	});
});

var _now = Date.now || function() { return new Date().getTime(); }
var _debounce = function(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	}
}



window.addEventListener("resize", _debounce(function() {
		googletag.pubads().refresh([window.slot1]);
	}, 100)
);


$(document).ready(function(){
	googletag.cmd.push(function() { googletag.display('div-gpt-ad-1418129986259-0'); });
	googletag.cmd.push(function() { googletag.display('div-gpt-ad-1421334354557-0'); });
});



