# S4C Listings #

This repo is for development of the 2016 S4C listings page

### To work on this repo: ###

* [Download Sourcetree](https://www.atlassian.com/software/sourcetree)
* Create a new branch on bitbucket by clicking the 'create branch' button on the left hand column
* Clone the repo in Sourcetree by clicking this button: 
![clone.png](https://bitbucket.org/repo/xbgRpj/images/1362824120-clone.png)
* Once your work is finished commit it in Sourcetree and push it to your remote branch
* Back on Bitbucket create a pull request by clicking this button: 
![pull.png](https://bitbucket.org/repo/xbgRpj/images/329577573-pull.png)
* Once changes have been checked and any conflicts resolved the branch can be merged with the master